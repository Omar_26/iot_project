#include <DFRobot_sim808.h>
#include <SoftwareSerial.h>
#include <WiFi.h>

#define PIN_TX    17
#define PIN_RX    16
#define MESSAGE_LENGTH 160
#define PHONE_NUMBER "3741173237" //Mobile phone number to send msg  

unsigned long count = 0;
char send_message[MESSAGE_LENGTH];
char latitude[12];
char longitude[12];
int tiempo_espera = 50000;/**Tiempo en milisegundos que espera al mensaje.*/

SoftwareSerial mySerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&mySerial);//Connect RX,TX,PWR,

void setup() 
{
  mySerial.begin(9600);
  Serial.begin(9600);
    //******** Initialize sim808 module *************
  while(!sim808.init()) 
  { 
      delay(1000);
      Serial.print("Sim808 init error\r\n");
  }

  //************* Turn on the GPS power************
  if( sim808.attachGPS())
      Serial.println("Open the GPS power success");
  else 
      Serial.println("Open the GPS power failure");
}

void loop() 
{
     //************** Get GPS data *******************
  if (sim808.getGPS()) 
  {
    Serial.print("latitude :");
    Serial.println(sim808.GPSdata.lat);
    Serial.print("longitude :");
    Serial.println(sim808.GPSdata.lon);

    //************* Turn off the GPS power ************
    float la = sim808.GPSdata.lat;
    float lo = sim808.GPSdata.lon;

    dtostrf(la, 4, 6, latitude); //put float value of la into char array of lat. 4 = number of digits before decimal sign. 6 = number of digits after the decimal sign.
    dtostrf(lo, 4, 6, longitude); //put float value of lo into char array of lon

    sprintf(send_message, "Latitude : %s\nLongitude: %s\n", latitude, longitude); 
  }
  /***/

  /**Envia el mensaje de texto con la ubicacion cada determinado tiempo.*/
  if (millis() - count > tiempo_espera)
  {// ajusta el valor de pausa que deses
    count = millis(); 
    Serial.println ("50 segundos");
   sim808.sendSMS(PHONE_NUMBER,send_message);
 }
}
