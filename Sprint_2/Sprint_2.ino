#include <DFRobot_sim808.h>
#include <SoftwareSerial.h>
#include <WiFi.h>
#include <FirebaseESP32.h>

#define MESSAGE_LENGTH 160
#define PHONE_NUMBER "3310512258" //Mobile phone number to send msg  
#define WIFI_SSID "NZT-2570"
#define WIFI_PASSWORD "74371402"

#define FIREBASE_HOST "iot-project-transport-tracker-default-rtdb.firebaseio.com" ////"sprint-2-a7b83-default-rtdb.firebaseio.com"

/** The database secret is obsoleted, please use other authentication methods, 
 * see examples in the Authentications folder. 
*/
#define FIREBASE_AUTH "h4o02JavPJLCfmR1EX87XDOgOEhMjL469Vdo7cHD"

//Define FirebaseESP32 data object
FirebaseData fbdo;

FirebaseJson json;

unsigned long count = 0;
void espera(int valor);

char received_message[MESSAGE_LENGTH];
char send_message[MESSAGE_LENGTH];
char latitude[12];
char longitude[12];
char wspeed[12];
char received_phone[16];
char datetime[24];
int messageIndex = 0;

#define PIN_TX    17
#define PIN_RX    16
SoftwareSerial mySerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&mySerial);//Connect RX,TX,PWR,

String path = "/Coordinate";
float la;
float lo;
float ws;

void setup() {
  mySerial.begin(9600);
  Serial.begin(9600);
  SetWifi();

  //******** Initialize sim808 module *************
  while(!sim808.init()) { 
      delay(1000);
      Serial.print("Sim808 init error\r\n");
  }

  //************* Turn on the GPS power************
  if( sim808.attachGPS())
      Serial.println("Open the GPS power success");
  else 
      Serial.println("Open the GPS power failure");
  
}

void SetWifi()
{
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to Wi-Fi");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(300);
  }
  Serial.println();
  Serial.print("Connected with IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);

  //Set database read timeout to 1 minute (max 15 minutes)
  Firebase.setReadTimeout(fbdo, 1000 * 60);
  //tiny, small, medium, large and unlimited.
  //Size and its write timeout e.g. tiny (1s), small (10s), medium (30s) and large (60s).
  Firebase.setwriteSizeLimit(fbdo, "tiny");  
}

void loop() {
   //************** Get GPS data *******************
   if (sim808.getGPS()) {
    Serial.print("latitude :");
    Serial.println(sim808.GPSdata.lat);
    Serial.print("longitude :");
    Serial.println(sim808.GPSdata.lon);
    
   la = sim808.GPSdata.lat;
   lo = sim808.GPSdata.lon;
   ws = sim808.GPSdata.speed_kph;

  dtostrf(la, 4, 6, latitude); //put float value of la into char array of lat. 4 = number of digits before decimal sign. 6 = number of digits after the decimal sign.
  dtostrf(lo, 4, 6, longitude); //put float value of lo into char array of lon
  dtostrf(ws, 6, 2, wspeed);  //put float value of ws into char array of wspeed

  sprintf(send_message, "Latitude : %s\nLongitude : %s\nWind Speed : %s My Module Is Working\n", latitude, longitude); 
  }
  /**Envia mensaje con los datos leidos cada 50 segundos*/
  if (millis() - count > 10000){ // En lugar de 10000, ajusta el valor de pausa que deses
  count = millis(); 
  Serial.println ("10 segundos");
  //sim808.sendSMS(PHONE_NUMBER,send_message);
  Firebase.setDouble(fbdo, path + "/lat", la);
  Firebase.setDouble(fbdo, path + "/lng", lo);
 }
}
