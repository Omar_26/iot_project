#include <DFRobot_sim808.h>
#include <SoftwareSerial.h>
#include <WiFi.h>
#include <FirebaseESP32.h>

#define MESSAGE_LENGTH 160
#define PHONE_NUMBER "3310512258" //Mobile phone number to send msg  
#define WIFI_SSID "NZT-2570"
#define WIFI_PASSWORD "74371402"

#define FIREBASE_HOST "iot-project-transport-tracker-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "h4o02JavPJLCfmR1EX87XDOgOEhMjL469Vdo7cHD"

#define PIN_TX    17
#define PIN_RX    16

//Define FirebaseESP32 data object
FirebaseData fbdo;

FirebaseJson json;

unsigned long count = 0;
void espera(int valor);
char received_message[MESSAGE_LENGTH];
char latitude[12];
char longitude[12];
char s_latitude[12];
char s_longitude[12];
char from_phone[16];
char datetime[24];
int messageIndex = 0;

String path = "/Coordinate";
float la;
float lo;
float ws;
double f;
double g;

SoftwareSerial mySerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&mySerial);//Connect RX,TX,PWR,

void setup() 
{
    mySerial.begin(9600);
  Serial.begin(9600);
  SetWifi();

  //******** Initialize sim808 module *************
  while(!sim808.init()) 
  { 
      delay(1000);
      Serial.print("Sim808 init error\r\n");
  }

}

void loop() 
{
   //*********** Detecting unread SMS ************************
   messageIndex = sim808.isSMSunread();
   if (messageIndex > 0)
   {
     readSMS();
     //Firebase.setString(fbdo,path + "/string",received_message );
     Firebase.setDouble(fbdo, path + "/lat", f);
     Firebase.setDouble(fbdo, path + "/lng", g);
   }
}

void readSMS()
{
  Serial.print("messageIndex: ");
  Serial.println(messageIndex);
  
  sim808.readSMS(messageIndex, received_message, MESSAGE_LENGTH, from_phone, datetime);
             
  //***********In order not to full SIM Memory, is better to delete it**********
  sim808.deleteSMS(messageIndex);
  Serial.print("From number: ");
  Serial.println(from_phone);  
  Serial.print("Datetime: ");
  Serial.println(datetime);        
  Serial.print("Recieved Message: ");
  Serial.println(received_message);
  for(int i = 0; i<11;i++)
  {
    s_longitude[i] = received_message[11 + i];
    s_latitude[i] = received_message[34 + i];
  }
  f = atof(s_longitude);
  g = atof(s_latitude);
  Serial.println(f,6);
  Serial.println(g,6);
  
}

void SetWifi()
{
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to Wi-Fi");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(300);
  }
  Serial.println();
  Serial.print("Connected with IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);

  //Set database read timeout to 1 minute (max 15 minutes)
  Firebase.setReadTimeout(fbdo, 1000 * 60);
  //tiny, small, medium, large and unlimited.
  //Size and its write timeout e.g. tiny (1s), small (10s), medium (30s) and large (60s).
  Firebase.setwriteSizeLimit(fbdo, "tiny");  
}
